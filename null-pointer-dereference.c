#include <stdio.h>

struct s {
	int i;
};

int null_check(struct s *sp)
{
	int n = sp->i;
	if (sp != NULL) {
		puts("good pointer");
	} else {
		puts("NULL pointer");
	}
	return n;
}

int main(void)
{
	struct s ss = { 0 };
	null_check(&ss);
	null_check(NULL);
}
/* TITLE: Never a <code>NULL</code> Moment */
/* CLASS: undefined */
/* COMP0: gcc -Wall -Wextra -o $cmd $src */
/* RUN0: ./$cmd */
/* COMP1: gcc -O3 -Wall -Wextra -o $cmd $src */
/* RUN1: ./$cmd */

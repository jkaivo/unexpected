#include <string.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	char *s = "placeholder for text to be copied later";
	strcpy(s, argv[argc - 1]);
	printf("%s\n", s);
}
/* TITLE: We'll Change This Later */
/* CLASS: undefined */
/* COMP0: gcc -Wall -Wextra -o $cmd $src */
/* RUN0: ./$cmd */
/* COMP1: tcc -Wall -Wextra -o $cmd $src */
/* RUN1: ./$cmd */

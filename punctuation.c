#include <ctype.h>
#include <locale.h>
#include <stdio.h>

int main(void)
{
	setlocale(LC_ALL, "");
	if (ispunct('�')) {
		puts("punctuation");
	} else {
		puts("not punctuation");
	}
}
/* TITLE: Character Classifier */
/* CLASS: locale-specific */
/* COMP0: gcc -Wall -Wextra -o $cmd $src */
/* RUN0: ./$cmd */
/* RUN1: LC_ALL=zh_CN.gbk ./$cmd */ 

#include <stdlib.h>
#include <string.h>

void secure_free(void *ptr, size_t n)
{
	memset(ptr, 0, n);
	free(ptr);
}
/* TITLE: Secure Free */
/* CLASS: optimized */
/* COMP0: gcc -S -O3 -Wall -Wextra $src */
/* RUN0: grep -Ef asm.grep $cmd.s */

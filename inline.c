#include <stdio.h>

static inline int hello(void)
{
	return puts("hello, world");
}

int main(void)
{
	hello();
}
/* TITLE: Get <code>inline</code> */
/* CLASS: unspecified */
/* COMP0: gcc -Wall -Wextra -S -masm=intel $src */
/* RUN0: grep -Ef asm.grep $cmd.s */
/* COMP1: gcc -O3 -Wall -Wextra -S -masm=intel $src */
/* RUN1: grep -Ef asm.grep $cmd.s */

function begin() {
	var slides = document.getElementsByTagName("div");

	var hash = document.location.hash.substring(1);
	if (hash == "") {
		document.location.hash = slides[0].id;
	}

	window.onkeydown = function(e) {
		//console.log(e);

		var hash = document.location.hash.substring(1);
		if (hash == "") {
			var slides = document.getElementsByTagName("div");
			document.location.hash = slides[0].id;
		}

		var current = document.getElementById(hash);
		var next = current.nextElementSibling;
		var prev = current.previousElementSibling;

		switch (e.key) {
		case "Home":
			var slides = document.getElementsByTagName("div");
			document.location.hash = slides[0].id;
			break;

		case ' ':
		case 'n':
		case "PageDown":
		case "ArrowDown":
		case "ArrowRight":
			var spans = current.getElementsByTagName("span");
			if (spans.length > 0) {
				var h1 = current.getElementsByTagName("h1");
				h1[0].className = "revealed";
				
				for (var i = 0; i < spans.length; i++) {
					if (spans[i].className == "") {
						spans[i].className = "revealed";
						return;
					}
				}
			}

			if (next) {
				document.location.hash = next.id;
			}
			break;

		case 'p':
		case "Backspace":
		case "PageUp":
		case "ArrowUp":
		case "ArrowLeft":
			if (prev) {
				document.location.hash = prev.id;
			}
			break;

		case "End":
			var slides = document.getElementsByTagName("div");
			document.location.hash = slides[slides.length-1].id;
			break;

		default:
			break;
		}
	}
}

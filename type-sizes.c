#include <stdio.h>

int main(void)
{
	printf("char:  %d\n", (int)sizeof(char));
	printf("short: %d\n", (int)sizeof(short));
	printf("int:   %d\n", (int)sizeof(int));
	printf("long:  %d\n", (int)sizeof(long));
}
/* TITLE: How <code>long</code> is a Type? */
/* CLASS: implementation-defined */
/* COMP0: gcc -Wall -Wextra -o $cmd $src */
/* RUN0: ./$cmd */
/* COMP1: i686-linux-gnu-gcc -Wall -Wextra -o $cmd $src */
/* RUN1: ./$cmd */
/* COMP2: x86_64-w64-mingw32-gcc -Wall -Wextra -o $cmd.exe $src */
/* RUN2: wine ./$cmd.exe */

#include <stdio.h>

double log(double n)
{
	printf("LOG: %g\n", n);
	return n;
}

int main(void)
{
	printf("%g\n", log(100.0));
}
/* TITLE: Custom Logger */
/* CLASS: undefined */
/* COMP0: gcc -Wall -Wextra -o $cmd $src */
/* RUN0: ./$cmd */

int main(void)
{
}
/* TITLE: No Return */
/* CLASS: other */
/* COMP0: gcc -std=c89 -Wall -Wextra -o $cmd $src */
/* RUN0: ./$cmd ; echo $? */
/* COMP1: gcc -std=c99 -Wall -Wextra -o $cmd $src */
/* RUN1: ./$cmd ; echo $? */

#include <stdio.h>

int this(void)
{
	puts("This");
	return 1;
}

int is(void)
{
	puts("is");
	return 2;
}

int odd(void)
{
	puts("odd");
	return 3;
}

int main(void)
{
	printf("%d %d %d\n", this(), is(), odd());
}
/* TITLE: Order, Please */
/* CLASS: unspecified */
/* COMP0: gcc -Wall -Wextra -o $cmd $src */
/* RUN0: ./$cmd */
/* COMP1: clang -Wall -Wextra -o $cmd $src */
/* RUN1: ./$cmd */

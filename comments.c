#include <stdio.h>

int main(void)
{
	//// Outputs Hello \\\\
	puts("Hello");
	/*** Outputs Goodbye ***/
	puts("Goodbye");
}
/* TITLE: Comment Your Code */
/* CLASS: other */
/* COMP0: gcc -Wall -Wextra -o $cmd $src */
/* RUN0: ./$cmd */

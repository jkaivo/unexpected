#include <stdio.h>

int *a(void)
{
	int n = 42;
	return &n;
}

int main(void)
{
	int *ptr = a();
	printf("%d\n", *ptr);
}
/* TITLE: Return of No Point(er) */
/* CLASS: undefined */
/* COMP0: gcc -Wall -Wextra -o $cmd $src */
/* RUN0: ./$cmd */

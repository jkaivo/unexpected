#include <stdio.h>

#define A_STRING "a string"

int main(void)
{
	printf("s1: %p\n", (void*)A_STRING);
	printf("s2: %p\n", (void*)A_STRING);
}
/* TITLE: Twin Strings */
/* CLASS: unspecified */
/* COMP0: gcc -Wall -Wextra -o $cmd $src */
/* RUN0: ./$cmd */
/* COMP1: tcc -Wall -Wextra -o $cmd $src */
/* RUN1: ./$cmd */

#include <stdio.h>

int foo(int i)
{
	int n = i * 42;
}

int main(void)
{
	printf("%d\n", foo(42));
}
/* TITLE: Point of No Return */
/* CLASS: undefined */
/* COMP0: gcc -o $cmd $src */
/* RUN0: ./$cmd */
/* COMP1: clang -o $cmd $src */
/* RUN1: ./$cmd */
/* COMP2: gcc -Wall -Wextra -o $cmd $src */
/* RUN2: */

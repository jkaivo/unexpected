.POSIX:

.SUFFIXES: .c .html

OUTPUT=unexpected.html

all: $(OUTPUT)

SLIDES=\
 title.html\
 agenda.html\
 unspecified.html\
 implementation-defined.html\
 locale-specific.html\
 undefined.html\
 other.html\
 examples.html\
 missing-return-statement.html\
 no-return.html\
 lifetime.html\
 punctuation.html\
 string-sort.html\
 type-sizes.html\
 order-of-evaluation.html\
 null-pointer-dereference.html\
 secure-free.html\
 identical-string-literals.html\
 inline.html\
 use-after-free.html\
 double-free.html\
 reserved.html\
 modify-string-literal.html\
 trigraphs.html\
 comments.html\
 pragma.html\
 resources.html\
 address.html

GETCLASS="$$(grep CLASS $< | sed -e 's/^.*: *//;s/ *\*\///')"
GETTITLE="$$(grep TITLE $< | sed -e 's/^.*: *//;s/ *\*\///')"

$(OUTPUT): style.css script.js $(SLIDES)
	printf '<!DOCTYPE html>\n<html lang="en">\n' > $@
	printf '<head>\n<meta charset="iso-8859-1">\n' >> $@
	printf '<title>Unexpected Behaviors</title>\n<style>\n' >> $@
	cat style.css >> $@
	printf '</style>\n<script>\n' >> $@
	cat script.js >> $@
	printf '</script>\n</head>\n<body onload="begin();">\n' >> $@
	cat $(SLIDES) >> $@
	printf '</body>\n</html>\n' >> $@

.c.html:
	printf '<div id="$*" class="%s">\n' $(GETCLASS) > $@
	printf '<h1>%s</h1>\n<pre><code>' $(GETTITLE) >> $@
	grep -v '^\/\*' $< | sed -e 's/&/\&amp;/g;s/</\&lt;/g;s/>/\&gt;/g' >> $@
	printf '</code></pre>\n' >> $@
	sh buildandrun $< >> $@
	printf '</div>\n' >> $@

clean:
	rm -f $(OUTPUT) $$(ls -1 *.c | sed -e 's/\.c$$/.html/g' | tr '\n' ' ') $$(cat .gitignore)

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	char *ptr = malloc(32);
	sprintf(ptr, "hello, world");
	printf("%s\n", ptr);
	free(ptr);
	printf("%s\n", ptr);

	ptr = malloc(65536);
	sprintf(ptr, "hello, again");
	printf("%s\n", ptr);
	free(ptr);
	printf("%s\n", ptr);

	printf("goodbye\n");
}
/* TITLE: Free Real Estate */
/* CLASS: undefined */
/* COMP0: gcc -Wall -Wextra -o $cmd $src */
/* RUN0: ./$cmd */

#include <locale.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int compare(const void *s1, const void *s2)
{
	return strcoll(*(char *const *)s1, *(char *const *)s2);
}

int main(void)
{
	char *words[] = { "cart", "Horse" };
	setlocale(LC_ALL, "");

	qsort(words, sizeof(words) / sizeof(words[0]), sizeof(words[0]), compare);

	for (size_t i = 0; i < sizeof(words) / sizeof(words[0]); i++) {
		puts(words[i]);
	}
}
/* TITLE: Putting the Cart Before the Horse */
/* CLASS: locale-specific */
/* COMP0: gcc -Wall -Wextra -o $cmd $src */
/* RUN0: LC_ALL=C ./$cmd */
/* RUN1: LC_ALL=en_US ./$cmd */

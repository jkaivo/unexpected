#include <stdlib.h>

int main(void)
{
	void *ptr = malloc(16);
	free(ptr);
	free(ptr);
}
/* TITLE: Really Free */
/* CLASS: undefined */
/* COMP0: gcc -Wall -Wextra -o $cmd $src */
/* RUN0: ./$cmd */
